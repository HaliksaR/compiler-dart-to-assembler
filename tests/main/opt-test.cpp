#include "gtest/gtest.h"
#include "main/optionsParser/OptionsParser.h"

class TestMainArgs : public ::testing::Test {
public:
    TestMainArgs() { /* init protected members here */ }

    ~TestMainArgs() { /* free protected members here */ }

    void SetUp() { /* called before every test */ }

    void TearDown() { /* called after every test */ }

protected:
    /* none yet */
};

namespace {
    TEST(TestMainArgs, Test_main_args_success) {
        std::vector<Dump> dump_state;
        std::string path;
        char const *argv[] = {
                "compiler", // default
                "--dump-tokenManager",
                "--dump-ast",
                "--dump-asm",
                "../program/main.dart"
        };
        int argc = 5;
        EXPECT_EQ (read_args(argc, argv, path, dump_state), Status::SUCCESS);
    }

    TEST(TestMainArgs, Test_main_args_no_path) {
        std::vector<Dump> dump_state;
        std::string path;
        char const *argv[] = {
                "compiler", // default
                "--dump-tokenManager",
                "--dump-ast",
                "--dump-asm"
        };
        int argc = 4;
        EXPECT_EQ (read_args(argc, argv, path, dump_state), Status::ERROR);
    }

    TEST(TestMainArgs, Test_main_args_invalid) {
        std::vector<Dump> dump_state;
        std::string path;
        char const *argv[] = {
                "compiler", // default
                "--dump-tokenManager",
                "--dump-aghfghst",
                "--dump-asm",
                "../program/main.dart"
        };
        int argc = 4;
        EXPECT_EQ (read_args(argc, argv, path, dump_state), Status::ERROR);
    }

    TEST(TestMainArgs, Test_main_args_invalid2) {
        std::vector<Dump> dump_state;
        std::string path;
        char const *argv[] = {
                "compiler", // default
                "--dump-tokenManager",
                "fghfghfh",
                "--dump-asm",
                "../program/main.dart"
        };
        int argc = 4;
        EXPECT_EQ (read_args(argc, argv, path, dump_state), Status::ERROR);
    }
}