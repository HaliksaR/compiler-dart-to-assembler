#include "gtest/gtest.h"
#include "frontend/Lex/Lexer/Lexer.h"
#include "frontend/Lex/Token/Token.h"
#include <enums/Status.h>
#include <enums/Dump.h>
#include <frontend/Lex/PrintDump/PrintDump.h>
#include <frontend/Syntax/Parser/Parser.h>
#include <frontend/Syntax/PrintDump/PrintDump.h>

TEST(TestParser, TestParser_test) {
    std::pair<std::ifstream, int> Args[]{
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test1.dart"),
                    0
            ),
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test2.dart"),
                    2
            ),
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test3.dart"),
                    1
            ),
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test4.dart"),
                    0
            ),
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test5.dart"),
                    1
            ),
            std::pair<std::ifstream, int>(
                    std::ifstream("../../tests/compiler/testParser/test6.dart"),
                    3
            )
    };
    std::vector<Dump> dump_state;

    for (int i = 0; i < sizeof(Args) / sizeof(Args[0]); i++) {
        std::cout << "test " << i + 1 << std::endl << std::endl;
        auto *resultLexer = new std::vector<Token>();
        auto *temp = new std::vector<Token>();
        auto *lexer = new Lexer(Args[i].first);
        lexer->parse(temp);
        int countUnknown;
        if (dump_lexer(temp, dump_state, countUnknown) == Status::ERROR)
            exit(EXIT_FAILURE);
        for (auto &token : *temp)
            if (token.getType() != Token::CommentText)
                resultLexer->push_back(token);

        auto *parser = new Parser(resultLexer);
        parser->parse();
        unsigned int count = parser->getCountErrors();
        if (parser->getCountErrors() != 0) {
            auto map = parser->getErrors();
            std::for_each(map.begin(), map.end(), [](const std::pair<Token *, std::string> &error) {
                std::cout << error.second << std::endl;
                std::cout << "->" << error.first->getLexeme() << "<- " << getPositions(error.first->getPosition())
                          << std::endl;
            });
            std::cout << "count -> " << count << std::endl;
        } else {

        }
        EXPECT_EQ (count, Args[i].second);
        Args[i].first.close();
        free(lexer);
    }
}