#include "gtest/gtest.h"
#include "frontend/Lex/Lexer/Lexer.h"
#include "frontend/Lex/Token/Token.h"
#include <enums/Status.h>
#include <enums/Dump.h>
#include <frontend/Lex/PrintDump/PrintDump.h>

TEST(TestLexer, TestLexer_test) {
    std::pair<std::ifstream, Status> Args[]{
            std::pair<std::ifstream, Status>(
                    std::ifstream("../../tests/compiler/testLexer/test1.dart"),
                    Status::SUCCESS
            ),
            std::pair<std::ifstream, Status>(
                    std::ifstream("../../tests/compiler/testLexer/test2.dart"),
                    Status::SUCCESS
            ),
            std::pair<std::ifstream, Status>(
                    std::ifstream("../../tests/compiler/testLexer/test3.dart"),
                    Status::ERROR
            ),
            std::pair<std::ifstream, Status>(
                    std::ifstream("../../tests/compiler/testLexer/test4.dart"),
                    Status::SUCCESS
            ),
            std::pair<std::ifstream, Status>(
                    std::ifstream("../../tests/compiler/testLexer/test5.dart"),
                    Status::ERROR
            )
    };
    for (int i = 0; i < sizeof(Args) / sizeof(Args[0]); i++) {
        std::cout << "test " << i + 1 << std::endl;
        int count;
        std::vector<Dump> dump_state;
        auto *resultLexer = new std::vector<Token>();
        auto lex = new Lexer(Args[i].first);
        lex->parse(resultLexer);
        EXPECT_EQ (dump_lexer(resultLexer, dump_state, count), Args[i].second);
        Args[i].first.close();
        free(lex);
    }
}