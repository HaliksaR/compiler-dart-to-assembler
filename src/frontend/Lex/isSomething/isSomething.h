#pragma once

bool isIdentifier(char arg);

bool isSpace(char arg);

bool isDigit(char arg);

bool isHex(char arg);