#include "PrintDump.h"
#include <iostream>
#include <algorithm>
#include <main/colors.h>
#include <sstream>
#include <fstream>
#include "enums/converter_enum.h"

void printDump(Token token) {
    std::cout << "<type=\"" << GREEN << token.getType() << RESET << "\">" << YELLOW << token.getLexeme()
              << RESET << "<chars=" << std::to_string(token.getLexeme().length()) << " positions=[" <<
              positionString(token, false) << "]>\n";
}

std::string getDump(Token token) {
    std::stringstream ss;
    ss << "<type=\"" << token.getType()  << "\">"<< token.getLexeme()
        << "<chars=" << std::to_string(token.getLexeme().length()) << " positions=[" <<
       positionString(token, false) << "]>\n";
    return ss.str();
}

void printError(Token token) {
    std::cout << "<type=\"" << RED << token.getType() << RESET << "\">" << YELLOW << token.getLexeme() << RESET
              << "<chars=" << std::to_string(token.getLexeme().length()) << " positions=[" <<
              positionString(token, false) << "]>\n";
}

std::string positionString(const Token &token, bool onlyStart) {
    std::string ss = std::to_string(token.getStartPosition().line);
    ss += ":";
    ss += std::to_string(token.getStartPosition().column);
    if (onlyStart)
        return ss;
    ss += "|";
    ss += std::to_string(token.getEndPosition().line);
    ss += ":";
    ss += std::to_string(token.getEndPosition().column);
    return ss;
}

Status dump_lexer(const std::vector<Token> *result, const std::vector<Dump> &state, int &countUnknown) {
    countUnknown = 0;
    Status errorState = Status::SUCCESS;
    if (std::find(state.begin(), state.end(), Dump::DUMP_TOKENS) != state.end()) {
        for (Token token : *result) {
            if (token.is(Token::Type::Unknown)) {
                printError(token);
                countUnknown++;
                errorState = Status::ERROR;
                continue;
            }
            printDump(token);
        }
    } else if (std::find(state.begin(), state.end(), Dump::DUMP_TOKENS_NOT_COMMENTS) != state.end()) {
        for (Token token : *result) {
            if (token.is(Token::Type::CommentText)) {
                continue;
            }
            if (token.is(Token::Type::Unknown)) {
                printError(token);
                countUnknown++;
                errorState = Status::ERROR;
                continue;
            }
            printDump(token);
        }
    } else if (std::find(state.begin(), state.end(), Dump::DUMP_TOKENS_SAVE) != state.end()) {
        std::ofstream outfile ("lexer_dump.txt");
        for (Token token : *result) {
            if (token.is(Token::Type::Unknown)) {
                printError(token);
                countUnknown++;
                errorState = Status::ERROR;
            }
            outfile << getDump(token);
        }
        outfile.close();
    } else {
        for (Token token : *result) {
            if (token.is(Token::Type::Unknown)) {
                printError(token);
                countUnknown++;
                errorState = Status::ERROR;
                continue;
            }
        }
    }
    return errorState;
}