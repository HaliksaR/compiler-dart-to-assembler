#pragma once

#include <enums/Dump.h>
#include <enums/Status.h>
#include <vector>
#include <frontend/Lex/Token/Token.h>

void printError(Token token);

void printDump(Token token);

std::string positionString(const Token &token, bool onlyStart);

Status dump_lexer(const std::vector<Token> *result, const std::vector<Dump> &state, int &countUnknown);