#pragma once

#include <frontend/Lex/Position/Positions.h>
#include <fstream>
#include <vector>
#include <frontend/Lex/Token/Token.h>

class Lexer {
public:
    explicit Lexer(std::ifstream &file);

    bool parse(std::vector<Token> *result);

    ~Lexer();

private:

    std::ifstream &file;

    Positions positions;

    std::string buffer;

    void initPositions();

    void increaseLine();

    void decreaseLine();

    void increaseColumn();

    void decreaseColumn();

    void cleanColumn();

    void refreshStart();


    Token getNextToken();

    char peekToken();

    char getToken();

    void unGetToken(int count);

    Token comment();

    Token singleLineCommit();

    Token identifierType();

    Token alone(Token::Type chunk);

    Token identifier();

    Token intOrDoubleValue();

    Token operation_or_comment_or_comparison();

    Token string();

    Token operation();

    Token numberValue();

    Token hexValue();

    Token doubleValue();

    Token double_or_alone();

    Token operation_or_comparison();
};