#include <vector>
#include <algorithm>
#include "Lexer.h"
#include "frontend/Lex/isSomething/isSomething.h"

Lexer::Lexer(std::ifstream &file) : file(file) {}

bool Lexer::parse(std::vector<Token> *result) {
    initPositions();
    Token token;
    do {
        token = getNextToken();
        result->push_back(token);
    } while (!token.is(Token::Type::End));
    return true;
}

Lexer::~Lexer() {
    file.close();
}

Token Lexer::getNextToken() {
    while (isSpace(peekToken())) getToken();
    refreshStart();
    buffer.clear();
    buffer += peekToken();
    switch (peekToken()) {
        case '\0':
        case EOF:
            return Token(Token::Type::End, buffer, positions);
        default:
            return alone(Token::Type::Unknown);
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
            return identifier();
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            return numberValue();
        case '(':
            return alone(Token::Type::LParen);
        case ')':
            return alone(Token::Type::RParen);
        case '[':
            return alone(Token::Type::LBox);
        case ']':
            return alone(Token::Type::RBox);
        case '{':
            return alone(Token::Type::LFigured);
        case '}':
            return alone(Token::Type::RFigured);
        case '<':
        case '>':
        case '=':
        case '+':
        case '-':
        case '*':
        case '!':
            return operation_or_comparison();
        case '/':
            return operation_or_comment_or_comparison();
        case '.':
            return double_or_alone();
        case ',':
            return alone(Token::Type::Comma);
        case ':':
            return alone(Token::Type::Colon);
        case ';':
            return alone(Token::Type::Semicolon);
        case '\'':
        case '"':
            return string();
    }
}

Token Lexer::identifier() {
    getToken();
    while (isIdentifier(peekToken())) {
        buffer += peekToken();
        getToken();
    }
    return identifierType();
}

Token Lexer::identifierType() {
    if (buffer == "return")
        return Token(Token::Type::KeywordReturn, buffer, positions);
    if (buffer == "if")
        return Token(Token::Type::KeywordIf, buffer, positions);
    if (buffer == "else")
        return Token(Token::Type::KeywordElse, buffer, positions);
    if (buffer == "for")
        return Token(Token::Type::KeywordFor, buffer, positions);
    if (buffer == "import")
        return Token(Token::Type::KeywordImport, buffer, positions);
    if (buffer == "double")
        return Token(Token::Type::TypeDouble, buffer, positions);
    if (buffer == "void")
        return Token(Token::Type::TypeVoid, buffer, positions);
    if (buffer == "List")
        return Token(Token::Type::TypeList, buffer, positions);
    if (buffer == "int")
        return Token(Token::Type::TypeInt, buffer, positions);
    if (buffer == "String")
        return Token(Token::Type::TypeString, buffer, positions);
    if (buffer == "var")
        return Token(Token::Type::KeywordVar, buffer, positions);
    if (buffer == "bool")
        return Token(Token::Type::TypeBoolean, buffer, positions);
    if (buffer == "true")
        return Token(Token::Type::BooleanValueTrue, buffer, positions);
    if (buffer == "false")
        return Token(Token::Type::BooleanValueFalse, buffer, positions);
    if (buffer == "null")
        return Token(Token::Type::NullValue, buffer, positions);
    if (buffer == "while")
        return Token(Token::Type::KeywordWhile, buffer, positions);
    if (buffer == "do")
        return Token(Token::Type::KeywordDo, buffer, positions);
    return Token(Token::Type::Identifier, buffer, positions);
}

// а куда же делась восьмеричка? а вот
// https://pub.dev/documentation/quantity/latest/number/Octal-class.html
Token Lexer::numberValue() {
    getToken();
    if (peekToken() == 'x' || peekToken() == 'X') {
        return hexValue();
    }
    return intOrDoubleValue();
}

Token Lexer::double_or_alone() {
    getToken();
    if (isDigit(peekToken())) {
        buffer += peekToken();
        return doubleValue();
    }
    unGetToken(1);
    return alone(Token::Type::Dot);
}

Token Lexer::hexValue() {
    buffer += peekToken();
    getToken();
    for (; isHex(peekToken());) {
        buffer += peekToken();
        getToken();
    }
    if (buffer.length() - 2 > 16)
        return Token(Token::Type::Unknown, buffer, positions);
    return Token(Token::Type::IntValue, buffer, positions);
    // почему инт? а вот
    // https://pub.dev/documentation/quantity/latest/number/Hexadecimal-class.html
}

Token Lexer::intOrDoubleValue() {
    for (; isDigit(peekToken());) {
        buffer += peekToken();
        getToken();
    }
    if (peekToken() == '.') {
        buffer += peekToken();
        return doubleValue();
    }
    if (buffer.length() - std::count(buffer.begin(), buffer.end(), '0') > 20) {
        return Token(Token::Type::Unknown, buffer, positions);
    } // в оригинале дабл п*ц какой большой, и дарту параллельно на колво ноликов в начале
    // https://dart.dev/articles/archive/numeric-computation
    // https://flutterrdart.com/dart-data-types-and-variables-tutorial-with-examples/
    return Token(Token::Type::IntValue, buffer, positions);
}

Token Lexer::doubleValue() {
    getToken();
    if (isDigit(peekToken())) {
        for (; isDigit(peekToken());) {
            buffer += peekToken();
            getToken();
        }
        return Token(Token::Type::DoubleValue, buffer, positions);
    }
    unGetToken(1);
    buffer.pop_back();
    return Token(Token::Type::IntValue, buffer, positions);
}

char Lexer::peekToken() {
    return file.peek();
}

char Lexer::getToken() {
    if (file.peek() == '\n') {
        increaseLine();
        cleanColumn();
    } else {
        increaseColumn();
    }
    return file.get();
}

void Lexer::unGetToken(const int count) {
    for (int i = 0; i < count; ++i) {
        file.unget();
        if (file.peek() == '\n') {
            decreaseLine();
            cleanColumn();
        } else {
            decreaseColumn();
        }
    }
}

Token Lexer::alone(Token::Type type) {
    getToken();
    return Token(type, buffer, positions);
}

Token Lexer::operation_or_comment_or_comparison() {
    getToken();
    switch (peekToken()) {
        case '/': {
            return singleLineCommit();
        }
        case '*': {
            return comment();
        }
        default: {
            unGetToken(1);
            return operation_or_comparison();
        }
    }
}

Token Lexer::comment() {
    buffer += peekToken();
    getToken();
    while (peekToken() != '\0' && peekToken() != EOF) {
        buffer += peekToken();
        if (getToken() == '*') {
            buffer += peekToken();
            if (getToken() == '/') {
                buffer += peekToken();
                std::string temp;
                temp.assign(buffer, 2, buffer.length() - 4);
                temp.erase(std::remove(temp.begin(), temp.end(), '\n'), temp.end());
                temp.erase(std::remove(temp.begin(), temp.end(), '\t'), temp.end());
                temp.erase(std::remove(temp.begin(), temp.end(), '\r'), temp.end());
                return Token(Token::Type::CommentText, temp, positions);
            }
        }
    }
    return Token(Token::Type::Unknown, buffer, positions);
}

Token Lexer::singleLineCommit() {
    buffer += peekToken();
    getToken();
    while (peekToken() != '\0') {
        buffer += peekToken();
        if (getToken() == '\n') {
            std::string temp;
            temp.assign(buffer, 2, buffer.length() - 2);
            temp.erase(std::remove(temp.begin(), temp.end(), '\n'), temp.end());
            temp.erase(std::remove(temp.begin(), temp.end(), '\t'), temp.end());
            temp.erase(std::remove(temp.begin(), temp.end(), '\r'), temp.end());
            return Token(Token::Type::CommentText, temp, positions);
        }
    }
    return Token(Token::Type::Unknown, buffer, positions);
}


Token Lexer::operation_or_comparison() {
    getToken();
    switch (peekToken()) {
        case '=': {
            buffer += peekToken();
            getToken();
            if (buffer == "==")
                return Token(Token::Type::EqualTo, buffer, positions);
            if (buffer == "+=")
                return Token(Token::Type::AddAndAssignToSelf, buffer, positions);
            if (buffer == "-=")
                return Token(Token::Type::SubtractAndAssignToSelf, buffer, positions);
            if (buffer == "*=")
                return Token(Token::Type::MultiplyAndAssignToSelf, buffer, positions);
            if (buffer == "/=")
                return Token(Token::Type::DivideAndAssignToSelf, buffer, positions);
            if (buffer == "<=")
                return Token(Token::Type::LessThanOrEqualTo, buffer, positions);
            if (buffer == ">=")
                return Token(Token::Type::GreaterThanOrEqualTo, buffer, positions);
            if (buffer == "!=")
                return Token(Token::Type::NotEqualTo, buffer, positions);
        }
        case '+': {
            buffer += peekToken();
            getToken();
            if (buffer == "++")
                return Token(Token::Type::OperatorPlusPlus, buffer, positions);
        }
        case '-': {
            buffer += peekToken();
            getToken();
            if (buffer == "--")
                return Token(Token::Type::OperatorMinusMinus, buffer, positions);
        }
        default: {
            unGetToken(1);
            return operation();
        }
    }
}


Token Lexer::operation() {
    getToken();
    if (buffer == "+")
        return Token(Token::Type::OperatorPlus, buffer, positions);
    if (buffer == "/")
        return Token(Token::Type::OperatorDivision, buffer, positions);
    if (buffer == "-")
        return Token(Token::Type::OperatorMinus, buffer, positions);
    if (buffer == "=")
        return Token(Token::Type::OperatorAssignment, buffer, positions);
    if (buffer == "*")
        return Token(Token::Type::OperatorMultiply, buffer, positions);
    if (buffer == "<")
        return Token(Token::Type::LessThan, buffer, positions);
    if (buffer == ">")
        return Token(Token::Type::GreaterThan, buffer, positions);
    if (buffer == "!")
        return Token(Token::Type::OperatorExclamationMark, buffer, positions);
    return Token(Token::Type::Unknown, buffer, positions);
}

Token Lexer::string() {
    // https://www.tutorialspoint.com/dart_programming/dart_programming_string.htm
    const char l_limiter = peekToken();
    char errorLimiter = '\"';
    if (l_limiter == '\"') {
        errorLimiter = '\'';
    }
    getToken();
    buffer += peekToken();
    while (peekToken() != '\0' && peekToken() != '\n' && peekToken() != EOF) {
        if (peekToken() == l_limiter) {
            getToken();
            return Token(Token::Type::EmptyStringValue, buffer, positions);
        }
        getToken();
        buffer += peekToken();
        if (peekToken() == l_limiter) {
            getToken();
            return Token(Token::Type::StringValue, buffer, positions);
        } else if (peekToken() == errorLimiter) {
            getToken();
            return Token(Token::Type::Unknown, buffer, positions);
        }
    }
    getToken();
    buffer.erase(std::remove(buffer.begin(), buffer.end(), '\n'), buffer.end());
    return Token(Token::Type::Unknown, buffer, positions);
}

void Lexer::initPositions() {
    positions = Positions{Point{1, 1}, Point{1, 1}};
}

void Lexer::increaseLine() {
    positions.end.line++;
}

void Lexer::decreaseLine() {
    positions.end.line--;
}

void Lexer::increaseColumn() {
    positions.end.column++;
}

void Lexer::decreaseColumn() {
    positions.end.column--;
}

void Lexer::cleanColumn() {
    positions.end.column = 1;
}

void Lexer::refreshStart() {
    positions.start = positions.end;
}

