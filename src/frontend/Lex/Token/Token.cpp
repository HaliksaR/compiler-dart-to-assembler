#include "Token.h"

#include <utility>

Token::Token() = default;

Token::Token(Token::Type type, std::string lexeme, const Positions &positions) :
        type(type),
        lexeme(std::move(lexeme)),
        positions(positions) {}

const Token::Type &Token::getType() const {
    return type;
}


bool Token::is(Token::Type typeArg) {
    return type == typeArg;
}

const std::string &Token::getLexeme() {
    return lexeme;
}

const Positions &Token::getPosition() const {
    return positions;
}

const Point &Token::getStartPosition() const {
    return positions.start;
}

const Point &Token::getEndPosition() const {
    return positions.end;
}
