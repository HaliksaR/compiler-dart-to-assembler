#pragma once

#include <string_view>
#include <list>
#include <frontend/Lex/Position/Positions.h>
#include <string>

class Token {
public:
    enum Type {
        Identifier,

        KeywordVar,
        KeywordIf,
        KeywordElse,
        KeywordFor,
        KeywordImport,
        KeywordReturn,

        LParen,
        RParen,

        LFigured,
        RFigured,

        LBox,
        RBox,

        OperatorPlus,
        OperatorMinus,
        OperatorMultiply,
        OperatorDivision,
        OperatorAssignment,
        OperatorExclamationMark,

        LessThan,
        GreaterThan,

        Comma,
        Dot,
        Semicolon,
        Colon,

        TypeString,
        TypeList,
        TypeInt,
        TypeDouble,
        TypeVoid,
        TypeBoolean,

        StringValue,
        EmptyStringValue,
        IntValue,
        DoubleValue,
        BooleanValueTrue,
        BooleanValueFalse,
        NullValue,

        CommentText,

        LessThanOrEqualTo,
        EqualTo,
        NotEqualTo,
        AddAndAssignToSelf,
        SubtractAndAssignToSelf,
        MultiplyAndAssignToSelf,
        DivideAndAssignToSelf,
        GreaterThanOrEqualTo,

        OperatorPlusPlus,
        OperatorMinusMinus,

        End,
        Unknown,
        KeywordWhile,
        KeywordDo
    };

    Token();

    Token(Type type, std::string lexeme, const Positions &positions);

    [[nodiscard]] const Type &getType() const;

    const std::string &getLexeme();

    [[nodiscard]] const Positions &getPosition() const;

    [[nodiscard]] const Point &getStartPosition() const;

    [[nodiscard]] const Point &getEndPosition() const;

    bool is(Type typeArg);

private:
    Type type{};

    std::string lexeme;

    Positions positions{};

};