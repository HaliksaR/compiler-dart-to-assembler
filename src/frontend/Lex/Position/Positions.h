#pragma once

struct Point {
    long column;
    long line;
};

struct Positions {
    Point start;
    Point end;
};