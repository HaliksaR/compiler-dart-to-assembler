#include "PrintDump.h"
#include <algorithm>
#include <iostream>
#include <fstream>
[[nodiscard]] std::string getPositions(const Positions positions) {
    std::string ss = std::to_string(positions.start.line);
    ss += ":";
    ss += std::to_string(positions.start.column);
    ss += "|";
    ss += std::to_string(positions.end.line);
    ss += ":";
    ss += std::to_string(positions.end.column);
    return ss;
}

void dump_parser(AstNodeBase *result, const std::vector<Dump> &state) {
    if (std::find(state.begin(), state.end(), Dump::DUMP_AST) != state.end()) {
        auto visitor = new ParserVisitor();
        result->accept(visitor);
        std::cout << visitor->show() << std::endl;
    } else if(std::find(state.begin(), state.end(), Dump::DUMP_AST_SAVE) != state.end()) {
        std::ofstream outfile ("parser_dump.txt");
        auto visitor = new ParserVisitor();
        result->accept(visitor);
        outfile << visitor->show();
        outfile.close();
    }
}