#pragma once

#include <enums/Dump.h>
#include <enums/Status.h>
#include <vector>
#include <frontend/Lex/Token/Token.h>
#include <frontend/Syntax/AstTree/AstNodes.h>

[[nodiscard]] std::string getPositions(Positions positions);

void dump_parser(AstNodeBase *result, const std::vector<Dump> &state);