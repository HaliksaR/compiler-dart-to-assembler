#pragma once

#include <vector>
#include <frontend/Lex/Token/Token.h>

class TokenManager {
public:
    explicit TokenManager(std::vector<Token> *tokens);

    ~TokenManager();

    Token *get();

    Token *peek();

    Token *peek(int steps);

    Token *peekUnsafe(int steps);

    [[nodiscard]] unsigned int getPosition() const;

private:
    std::vector<Token> *tokens;
    unsigned int position;
};
