#include <iostream>
#include "TokenManager.h"

TokenManager::TokenManager(std::vector<Token> *tokens) : tokens(tokens), position(0) {}

TokenManager::~TokenManager() { delete tokens; }

Token *TokenManager::get() {
    if (position < tokens->size()) {
        Token *temp = &tokens->at(position);
        position++;
        return temp;
    }
    return nullptr;
}

Token *TokenManager::peek() {
    return peek(0);
}

Token *TokenManager::peek(int steps) {
    if (position + steps < tokens->size())
        return &tokens->at(position + steps);
    std::cerr << "Reached end of file while parsing" << std::endl;
    exit(EXIT_SUCCESS);
}

Token *TokenManager::peekUnsafe(int steps) {
    if (position + steps < tokens->size()) {
        return &tokens->at(position + steps);
    }
    return nullptr;
}

unsigned int TokenManager::getPosition() const {
    return position;
}




