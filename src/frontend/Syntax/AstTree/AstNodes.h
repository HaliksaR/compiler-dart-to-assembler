#pragma once

#include <frontend/Syntax/TokenManager/TokenManager.h>
#include <map>
#include <Visitor/Visitor.h>

class Visitor;

//----------------------------------------Base----------------------------------------------------
class AstNodeBase {
public:
    explicit AstNodeBase(TokenManager *_tokenManager, std::map<Token *, std::string> &errors) :
            errors(errors), tokenManager(_tokenManager) {}

    virtual ~AstNodeBase() {
        free(tokenManager);
        errors.clear();
    };

    virtual void *accept(Visitor *v) = 0;

    virtual bool parse() = 0;

protected:
    TokenManager *tokenManager;
    std::map<Token *, std::string> &errors;

    Token *match(Token::Type tokenType) {
        Token *token = tokenManager->get();
        if (token->getType() != tokenType) {
            errors.insert(std::make_pair(tokenManager->peek(), "AstNodeBase -> match"));
            return nullptr;
        } else {
            return token;
        }
    }
};
//-------------------------------------------------------------------------------------------------



//----------------------------------------Block----------------------------------------------------
class NodeBlock : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> statements;

    explicit NodeBlock(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeBlock() override {
        for(auto statement: statements) free(statement);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};
//-------------------------------------------------------------------------------------------------



//----------------------------------------Expressions----------------------------------------------------
class NodeExpression : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase *> relationalOperations;
    std::vector<AstNodeBase *> additiveExpressions;

    NodeExpression(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeExpression() override {
        for(auto op: relationalOperations) free(op);
        for(auto expr: additiveExpressions) free(expr);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeSubExpression : virtual public AstNodeBase {
public:
    AstNodeBase *expression = nullptr;

    NodeSubExpression(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeSubExpression() override {
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeAdditiveExpression : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase *> additiveOP;
    std::vector<AstNodeBase *> terms;

    NodeAdditiveExpression(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeAdditiveExpression() override {
        for(auto op: additiveOP) free(op);
        for(auto term: terms) free(term);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeUnaryPrefixExpression : virtual public AstNodeBase {
public:
    Token* token = nullptr;
    AstNodeBase* expression = nullptr;

    NodeUnaryPrefixExpression(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeUnaryPrefixExpression() override {
        free(token);
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeUnaryPostfixExpression : virtual public AstNodeBase {
public:
    Token* token = nullptr;
    AstNodeBase* expression = nullptr;

    NodeUnaryPostfixExpression(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeUnaryPostfixExpression() override {
        free(token);
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeIdentifierStatement : virtual public AstNodeBase {
public:
    AstNodeBase* identifier = nullptr;
    AstNodeBase* expression = nullptr;

    NodeIdentifierStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeIdentifierStatement() override {
        free(identifier);
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

//-------------------------------------------------------------------------------------------------



//----------------------------------------Factor----------------------------------------------------
class NodeFactor : virtual public AstNodeBase {
public:
    AstNodeBase *node = nullptr;

    NodeFactor(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeFactor() override {
        free(node);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};
//-------------------------------------------------------------------------------------------------



//----------------------------------------Term----------------------------------------------------
class NodeTerm : virtual public AstNodeBase {
public:

    std::vector<AstNodeBase *> factors;
    std::vector<AstNodeBase *> multiplicativeOP;

    NodeTerm(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeTerm() override {
        for (auto factor: factors) free(factor);
        for (auto op: multiplicativeOP) free(op);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};
//-------------------------------------------------------------------------------------------------

//----------------------------------------Functions----------------------------------------------------
class NodeFunctionDecl : virtual public AstNodeBase {
public:
    AstNodeBase* identifier = nullptr;
    AstNodeBase* formalParams = nullptr;
    AstNodeBase* block = nullptr;

    NodeFunctionDecl(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeFunctionDecl() override {
        free(identifier);
        free(formalParams);
        free(formalParams);
        free(block);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeFunctionCall : virtual public AstNodeBase {
public:
    AstNodeBase* actualParams = nullptr;
    AstNodeBase* expression = nullptr;

    NodeFunctionCall(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeFunctionCall() override {
        free(actualParams);
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeFieldCall : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;

    NodeFieldCall(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeFieldCall() override {
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeCallStatement : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;

    NodeCallStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeCallStatement() override {
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeCallParams : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> expressions;

    NodeCallParams(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeCallParams() override {
        for(auto expr: expressions) free(expr);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};


class NodeDeclParams : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> params;

    NodeDeclParams(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeDeclParams() override {
        for(auto param: params) free(param);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeDeclParam : virtual public AstNodeBase {
public:
    AstNodeBase* identifier = nullptr;
    AstNodeBase* type = nullptr;

    NodeDeclParam(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeDeclParam() override {
        free(identifier);
        free(type);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};
//-------------------------------------------------------------------------------------------------



//----------------------------------------Primitive----------------------------------------------------
class NodeType : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeType(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeType() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeLiteral : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeLiteral(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeLiteral() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeIdentifier : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeIdentifier(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeIdentifier() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeMultiplicativeOperation : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeMultiplicativeOperation(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeMultiplicativeOperation() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeAdditiveOperation : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeAdditiveOperation(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeAdditiveOperation() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeRelationalOperation : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeRelationalOperation(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeRelationalOperation() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeAssignmentOperation : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeAssignmentOperation(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeAssignmentOperation() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeUnaryPostfixOperation : virtual public AstNodeBase{
public:
    Token* token = nullptr;

    NodeUnaryPostfixOperation(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeUnaryPostfixOperation() override {
        free(token);
    }

    void *accept(Visitor *v) override;
    bool parse() override;
};

class NodeVariableCreate : virtual public AstNodeBase {
public:
    AstNodeBase* identifier = nullptr;
    AstNodeBase* value = nullptr;

    NodeVariableCreate(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeVariableCreate() override {
        free(identifier);
        free(value);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

//-------------------------------------------------------------------------------------------------



//----------------------------------------Root----------------------------------------------------
class NodeProgramState: virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> statements;
    AstNodeBase* imports = nullptr;

    NodeProgramState(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeProgramState() override {
        for (auto statement: statements) free(statement);
        free(imports);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeImports: virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> imports;

    NodeImports(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeImports() override {
        for (auto import: imports) free(import);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeImport: virtual public AstNodeBase {
public:
    Token *lib = nullptr;

    NodeImport(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeImport() override {
        free(lib);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};
//-------------------------------------------------------------------------------------------------



//----------------------------------------Statements----------------------------------------------------
class NodeStatement: virtual public AstNodeBase {
public:
    AstNodeBase *statement = nullptr;

    NodeStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeStatement() override {
        free(statement);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeRootStatement: virtual public AstNodeBase {
public:
    AstNodeBase *statement = nullptr;

    NodeRootStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeRootStatement() override {
        free(statement);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeTypesStatement: virtual public AstNodeBase {
public:
    AstNodeBase *statement = nullptr;
    AstNodeBase *type = nullptr;

    NodeTypesStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeTypesStatement() override {
        free(statement);
        free(type);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeVariableStatement : virtual public AstNodeBase {
public:
    AstNodeBase *statement = nullptr;

    NodeVariableStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeVariableStatement() override {
        free(statement);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeGenericStatement : virtual public AstNodeBase {
public:
    AstNodeBase *statement = nullptr;
    AstNodeBase *type = nullptr;

    NodeGenericStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeGenericStatement() override {
        free(statement);
        free(type);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeGenericsCreate : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> variables;

    NodeGenericsCreate(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeGenericsCreate() override {
        for (auto variable: variables) free(variable);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeGenericCreate : virtual public AstNodeBase {
public:
    AstNodeBase* collection = nullptr;
    AstNodeBase* identifier = nullptr;

    NodeGenericCreate(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeGenericCreate() override {
        free(collection);
        free(identifier);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeGenericDecl : virtual public AstNodeBase {
public:
    AstNodeBase* type = nullptr;
    AstNodeBase* identifier = nullptr;

    NodeGenericDecl(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeGenericDecl() override {
        free(type);
        free(identifier);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeCollection : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> variables;

    NodeCollection(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeCollection() override {
        for (auto variable: variables) free(variable);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeVariablesCreate : virtual public AstNodeBase {
public:
    std::vector<AstNodeBase*> variables;

    NodeVariablesCreate(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
    AstNodeBase(tokenManager, errors) {}

    ~NodeVariablesCreate() override {
        for (auto variable: variables) free(variable);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeIfStatement : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;
    AstNodeBase* block = nullptr;
    AstNodeBase* elseBlock = nullptr;
    AstNodeBase* elseIfBlock = nullptr;

    NodeIfStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeIfStatement() override {
        free(expression);
        free(block);
        free(elseBlock);
        free(elseIfBlock);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeForStatement : virtual public AstNodeBase {
public:
    AstNodeBase* variableDecl = nullptr; // optional
    AstNodeBase* expression = nullptr;
    AstNodeBase* assignment = nullptr; // optional
    AstNodeBase* block = nullptr; // optional

    NodeForStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeForStatement() override {
        free(variableDecl);
        free(expression);
        free(assignment);
        free(block);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeReturnStatement : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;

    NodeReturnStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeReturnStatement() override {
        free(expression);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeWhileStatement : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;
    AstNodeBase* block = nullptr;

    NodeWhileStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeWhileStatement() override {
        free(expression);
        free(block);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeDoWhileStatement : virtual public AstNodeBase {
public:
    AstNodeBase* block = nullptr;
    AstNodeBase* expression = nullptr;

    NodeDoWhileStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeDoWhileStatement() override {
        free(expression);
        free(block);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

class NodeAssignmentStatement : virtual public AstNodeBase {
public:
    AstNodeBase* expression = nullptr;
    AstNodeBase* operation = nullptr;

    NodeAssignmentStatement(TokenManager *tokenManager, std::map<Token *, std::string> &errors) :
            AstNodeBase(tokenManager, errors) {}

    ~NodeAssignmentStatement() override {
        free(expression);
        free(operation);
    }

    void *accept(Visitor *v) override;

    bool parse() override;
};

//-------------------------------------------------------------------------------------------------



//----------------------------------------Primitive----------------------------------------------------

//-------------------------------------------------------------------------------------------------



//----------------------------------------Primitive----------------------------------------------------