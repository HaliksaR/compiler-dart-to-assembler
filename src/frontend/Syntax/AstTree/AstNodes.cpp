#include <iostream>
#include "AstNodes.h"


bool NodeBlock::parse() {
    match(Token::LFigured);
    while (true) {
        if (tokenManager->peek()->getType() == Token::RFigured) {
            match(Token::RFigured);
            return true;
        }
        AstNodeBase *statement = new NodeStatement(tokenManager, errors);
        if (!statement->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeBlock -> NodeStatement"));
            return false;
        }
        statements.push_back(statement);
    }
}


bool NodeExpression::parse() {
    AstNodeBase *temp = new NodeAdditiveExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeExpression -> NodeAdditiveExpression"));
        return false;
    }
    additiveExpressions.push_back(temp);
    while (true) {
        AstNodeBase *operation, *expression;
        switch (tokenManager->peek()->getType()) {
            case Token::LessThan:
            case Token::GreaterThan:
            case Token::EqualTo:
            case Token::NotEqualTo:
            case Token::GreaterThanOrEqualTo:
            case Token::LessThanOrEqualTo:
                operation = new NodeRelationalOperation(tokenManager, errors);
                if (!operation->parse()) {
                    errors.insert(
                            std::make_pair(tokenManager->peek(), "ERROR in NodeExpression -> NodeRelationalOperation"));
                    return false;
                }
                relationalOperations.push_back(operation);
                expression = new NodeAdditiveExpression(tokenManager, errors);
                if (!expression->parse()) {
                    errors.insert(
                            std::make_pair(tokenManager->peek(), "ERROR in NodeExpression -> NodeAdditiveExpression"));
                    return false;
                }
                additiveExpressions.push_back(expression);
                break;
            default:
                return true;
        }
    }
}


bool NodeSubExpression::parse() {
    match(Token::LParen);
    AstNodeBase *expr = new NodeExpression(tokenManager, errors);
    if (!expr->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeSubExpression -> NodeExpression"));
        return false;
    }
    expression = expr;
    match(Token::RParen);
    return true;
}


bool NodeAdditiveExpression::parse() {
    AstNodeBase *temp = new NodeTerm(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeAdditiveExpression -> NodeTerm"));
        return false;
    }
    terms.push_back(temp);
    while (true) {
        AstNodeBase *operation, *term;
        switch (tokenManager->peek()->getType()) {
            case Token::OperatorPlus:
            case Token::OperatorMinus:
                operation = new NodeAdditiveOperation(tokenManager, errors);
                if (!operation->parse()) {
                    errors.insert(std::make_pair(tokenManager->peek(),
                                                 "ERROR in NodeAdditiveExpression -> NodeAdditiveOperation"));
                    return false;
                }
                additiveOP.push_back(operation);

                term = new NodeTerm(tokenManager, errors);
                if (!term->parse()) {
                    errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeAdditiveExpression -> NodeTerm"));
                    return false;
                }
                terms.push_back(term);
                break;
            default:
                return true;
        }
    }
}


bool NodeUnaryPrefixExpression::parse() {
    switch (tokenManager->peek()->getType()) {
        case Token::OperatorMinus:
            token = match(Token::OperatorMinus);
            break;
        case Token::OperatorExclamationMark:
            token = match(Token::OperatorExclamationMark);
            break;
        case Token::OperatorMinusMinus:
            token = match(Token::OperatorMinusMinus);
            break;
        case Token::OperatorPlusPlus:
            token = match(Token::OperatorPlusPlus);
            break;
        default:
            return false;
    }
    AstNodeBase *expr = new NodeExpression(tokenManager, errors);
    if (!expr->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeUnaryPrefixExpression -> NodeExpression"));
        return false;
    }
    expression = expr;
    return true;
}

bool NodeUnaryPostfixExpression::parse() {
    switch (tokenManager->peek()->getType()) {
        case Token::OperatorMinusMinus:
            token = match(Token::OperatorMinusMinus);
            break;
        case Token::OperatorPlusPlus:
            token = match(Token::OperatorPlusPlus);
            break;
        default:
            return false;
    }
    AstNodeBase *expr = new NodeExpression(tokenManager, errors);
    if (!expr->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeUnaryPostfixExpression -> NodeExpression"));
        return false;
    }
    expression = expr;
    return true;
}


bool NodeFactor::parse() {
    AstNodeBase *temp = nullptr;
    switch (tokenManager->peek()->getType()) {
        case Token::DoubleValue:
        case Token::IntValue:
        case Token::StringValue:
        case Token::EmptyStringValue:
        case Token::BooleanValueTrue:
        case Token::BooleanValueFalse:
        case Token::NullValue:
            temp = new NodeLiteral(tokenManager, errors);
            break;
        case Token::Identifier:
            temp = new NodeIdentifierStatement(tokenManager, errors);
            break;
        case Token::LParen:
            temp = new NodeSubExpression(tokenManager, errors);
            break;
        case Token::OperatorMinus:
        case Token::OperatorExclamationMark:
        case Token::OperatorMinusMinus:
        case Token::OperatorPlusPlus:
            temp = new NodeUnaryPrefixExpression(tokenManager, errors);
            break;
        case Token::Semicolon:
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFactor -> incorrect"));
            return false;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFactor -> parse"));
        return false;
    }
    node = temp;
    return true;
}


bool NodeIdentifierStatement::parse() {
    AstNodeBase *temp = new NodeIdentifier(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIdentifierStatement -> NodeIdentifier"));
        return false;
    }
    identifier = temp;
    temp = new NodeCallStatement(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIdentifierStatement -> NodeCallStatement"));
        return false;
    }
    expression = temp;
    return true;
}


bool NodeCallStatement::parse() {
    AstNodeBase *temp = nullptr;
    switch (tokenManager->peek()->getType()) {
        case Token::Dot:
            temp = new NodeFieldCall(tokenManager, errors);
            break;
        case Token::LParen:
            temp = new NodeFunctionCall(tokenManager, errors);
            break;
        case Token::OperatorAssignment:
        case Token::AddAndAssignToSelf:
        case Token::SubtractAndAssignToSelf:
        case Token::MultiplyAndAssignToSelf:
        case Token::DivideAndAssignToSelf:
            temp = new NodeAssignmentStatement(tokenManager, errors);
            break;
        case Token::OperatorPlusPlus:
        case Token::OperatorMinusMinus:
            temp = new NodeUnaryPostfixOperation(tokenManager, errors);
        default:
            return true;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeCallStatement -> parse"));
        return false;
    }
    expression = temp;
    return true;
}


bool NodeTerm::parse() {
    AstNodeBase *temp = new NodeFactor(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeTerm -> NodeFactor"));
        return false;
    }
    factors.push_back(temp);

    while (true) {
        AstNodeBase *operation, *factor;
        switch (tokenManager->peek()->getType()) {
            case Token::OperatorMultiply:
            case Token::OperatorDivision:
                operation = new NodeMultiplicativeOperation(tokenManager, errors);
                if (!operation->parse()) {
                    errors.insert(
                            std::make_pair(tokenManager->peek(), "ERROR in NodeTerm -> NodeMultiplicativeOperation"));
                    return false;
                }
                multiplicativeOP.push_back(operation);

                factor = new NodeFactor(tokenManager, errors);
                if (!factor->parse()) {
                    errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeTerm -> NodeFactor"));
                    return false;
                }
                factors.push_back(factor);
                break;
            default:
                return true;
        }
    }
}


bool NodeFunctionDecl::parse() {
    AstNodeBase *temp = new NodeIdentifier(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFunctionDecl -> NodeIdentifier"));
        return false;
    }
    identifier = temp;

    match(Token::LParen);
    if (tokenManager->peek()->getType() != Token::RParen) {
        temp = new NodeDeclParams(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFunctionDecl -> NodeDeclParams"));
            return false;
        }
        formalParams = temp;
    }
    match(Token::RParen);

    temp = new NodeBlock(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFunctionDecl -> NodeBlock"));
        return false;
    }
    block = temp;

    return true;
}


bool NodeFunctionCall::parse() {
    match(Token::LParen);
    AstNodeBase *temp = nullptr;
    if (tokenManager->peek()->getType() != Token::RParen) {
        temp = new NodeCallParams(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFunctionCall -> NodeCallParams"));
            return false;
        }
        actualParams = temp;
    }
    match(Token::RParen);

    temp = new NodeCallStatement(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFunctionCall -> NodeCallStatement"));
        return false;
    }
    expression = temp;

    return true;
}


bool NodeFieldCall::parse() {
    match(Token::Dot);
    AstNodeBase *temp = new NodeIdentifierStatement(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFieldCall -> NodeIdentifierStatement"));
        return false;
    }
    expression = temp;
    return true;
}


bool NodeCallParams::parse() {
    AstNodeBase *expr = new NodeExpression(tokenManager, errors);
    if (!expr->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFieldCall -> NodeExpression"));
        return false;
    }
    expressions.push_back(expr);

    while (tokenManager->peek()->getType() == Token::Comma) {
        match(Token::Comma);
        expr = new NodeExpression(tokenManager, errors);
        if (!expr->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeFieldCall -> NodeExpression"));
            return false;
        }
        expressions.push_back(expr);
    }
    return true;
}


bool NodeDeclParams::parse() {
    AstNodeBase *temp = new NodeDeclParam(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDeclParams -> NodeDeclParam"));
        return false;
    }
    params.push_back(temp);

    while (tokenManager->peek()->getType() == Token::Comma) {
        match(Token::Comma);
        temp = new NodeDeclParam(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDeclParams -> NodeDeclParam"));
            return false;
        }
        params.push_back(temp);
    }
    return true;
}


bool NodeDeclParam::parse() {
    AstNodeBase *temp = new NodeType(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDeclParam -> NodeType"));
        return false;
    }
    type = temp;

    switch (tokenManager->peek()->getType()) {
        case Token::Identifier:
            temp = new NodeIdentifier(tokenManager, errors);
            break;
        case Token::LessThan:
            temp = new NodeGenericDecl(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDeclParam -> incorrect"));
            return false;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDeclParam -> NodeIdentifier"));
        return false;
    }
    identifier = temp;
    return true;
}


bool NodeType::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::TypeDouble:
        case Token::TypeString:
        case Token::TypeBoolean:
        case Token::TypeInt:
        case Token::TypeVoid:
        case Token::TypeList:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeType -> incorrect"));
            return false;
    }
}


bool NodeLiteral::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::DoubleValue:
        case Token::IntValue:
        case Token::StringValue:
        case Token::EmptyStringValue:
        case Token::BooleanValueTrue:
        case Token::BooleanValueFalse:
        case Token::NullValue:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeLiteral -> incorrect"));
            return false;
    }
}


bool NodeIdentifier::parse() {
    token = match(Token::Identifier);
    return true;
}


bool NodeMultiplicativeOperation::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::OperatorMultiply:
        case Token::OperatorDivision:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeMultiplicativeOperation -> incorrect"));
            return false;
    }
}


bool NodeAdditiveOperation::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::OperatorPlus:
        case Token::OperatorMinus:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeAdditiveOperation -> incorrect"));
            return false;
    }
}


bool NodeRelationalOperation::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::LessThan:
        case Token::GreaterThan:
        case Token::EqualTo:
        case Token::NotEqualTo:
        case Token::GreaterThanOrEqualTo:
        case Token::LessThanOrEqualTo:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeRelationalOperation -> incorrect"));
            return false;
    }
}


bool NodeAssignmentOperation::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::AddAndAssignToSelf:
        case Token::SubtractAndAssignToSelf:
        case Token::MultiplyAndAssignToSelf:
        case Token::DivideAndAssignToSelf:
        case Token::OperatorAssignment:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeAssignmentOperation -> incorrect"));
            return false;
    }
}


bool NodeUnaryPostfixOperation::parse() {
    Token *t = tokenManager->peek();
    switch (t->getType()) {
        case Token::OperatorPlusPlus:
        case Token::OperatorMinusMinus:
            token = match(t->getType());
            return true;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeUnaryPostfixOperation -> incorrect"));
            return false;
    }
}


bool NodeProgramState::parse() {
    if (tokenManager->peek()->getType() == Token::KeywordImport) {
        AstNodeBase *temp = new NodeImports(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeProgramState -> NodeImports"));
            return false;
        }
        imports = temp;
    }
    while (tokenManager->peek()->getType() != Token::End) {
        AstNodeBase *statement = new NodeRootStatement(tokenManager, errors);
        if (!statement->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeProgramState -> NodeRootStatement"));
            return false;
        }
        statements.push_back(statement);
    }
    return true;
}


bool NodeImports::parse() {
    AstNodeBase *import = new NodeImport(tokenManager, errors);
    if (!import->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeImports -> NodeImport"));
        return false;
    }
    imports.push_back(import);
    match(Token::Semicolon);
    while (tokenManager->peek()->getType() == Token::KeywordImport) {
        import = new NodeImport(tokenManager, errors);
        if (!import->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeImports -> NodeImport"));
            return false;
        }
        imports.push_back(import);
        match(Token::Semicolon);
    }
    return true;
}


bool NodeImport::parse() {
    match(Token::KeywordImport);
    lib = match(Token::StringValue);
    return true;
}


bool NodeStatement::parse() {
    AstNodeBase *temp = nullptr;
    switch (tokenManager->peek()->getType()) {
        case Token::TypeString:
        case Token::TypeList:
        case Token::TypeInt:
        case Token::TypeDouble:
        case Token::TypeVoid:
        case Token::TypeBoolean:
        case Token::KeywordVar:
            temp = new NodeTypesStatement(tokenManager, errors);
            break;
        case Token::OperatorMinus:
        case Token::LParen:
        case Token::OperatorExclamationMark:
        case Token::OperatorPlusPlus:
        case Token::OperatorMinusMinus:
        case Token::Identifier:
            temp = new NodeExpression(tokenManager, errors);
            break;
        case Token::KeywordReturn:
            temp = new NodeReturnStatement(tokenManager, errors);
            break;
        case Token::KeywordIf:
            temp = new NodeIfStatement(tokenManager, errors);
            break;
        case Token::KeywordFor:
            temp = new NodeForStatement(tokenManager, errors);
            break;
        case Token::KeywordWhile:
            temp = new NodeWhileStatement(tokenManager, errors);
            break;
        case Token::KeywordDo:
            temp = new NodeDoWhileStatement(tokenManager, errors);
            break;
        case Token::LFigured:
            temp = new NodeBlock(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeStatement -> incorrect"));
            return false;

    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeStatement -> parse"));
        return false;
    }
    statement = temp;
    if (tokenManager->peek()->getType() == Token::Semicolon) {
        match(Token::Semicolon);
    }
    return true;
}


bool NodeRootStatement::parse() {
    AstNodeBase *temp = nullptr;
    switch (tokenManager->peek()->getType()) {
        case Token::TypeString:
        case Token::TypeList:
        case Token::TypeInt:
        case Token::TypeDouble:
        case Token::TypeVoid:
        case Token::TypeBoolean:
            temp = new NodeTypesStatement(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeRootStatement -> incorrect"));
            return false;

    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeRootStatement -> parse"));
        return false;
    }
    statement = temp;
    if (tokenManager->peek()->getType() == Token::Semicolon) {
        match(Token::Semicolon);
    }
    return true;
}


bool NodeTypesStatement::parse() {
    AstNodeBase *temp = nullptr;

    if (tokenManager->peek()->getType() == Token::KeywordVar) {
        match(Token::KeywordVar);
    } else {
        temp = new NodeType(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeTypesStatement -> NodeType"));
            return false;
        }
        type = temp;
    }

    switch (tokenManager->peek()->getType()) {
        case Token::Identifier:
            temp = new NodeVariableStatement(tokenManager, errors);
            break;
        case Token::LessThan:
            temp = new NodeGenericStatement(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeTypesStatement -> incorrect"));
            return false;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeTypesStatement -> parse"));
        return false;
    }
    statement = temp;

    return true;
}


bool NodeVariableStatement::parse() {
    AstNodeBase *temp = nullptr;
    switch (tokenManager->peek(1)->getType()) {
        case Token::OperatorAssignment:
        case Token::Comma:
        case Token::Semicolon:
            temp = new NodeVariablesCreate(tokenManager, errors);
            break;
        case Token::LParen:
            temp = new NodeFunctionDecl(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariableStatement -> incorrect"));
            return false;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariableStatement -> parse"));
        return false;
    }
    statement = temp;
    return true;
}


bool NodeGenericStatement::parse() {
    match(Token::LessThan);
    AstNodeBase *temp = new NodeType(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericStatement -> NodeType"));
        return false;
    }
    type = temp;
    match(Token::GreaterThan);

    switch (tokenManager->peek(1)->getType()) {
        case Token::OperatorAssignment:
        case Token::Comma:
        case Token::Semicolon:
            temp = new NodeGenericsCreate(tokenManager, errors);
            break;
        case Token::LParen:
            temp = new NodeFunctionDecl(tokenManager, errors);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericStatement -> incorrect"));
            return false;
    }
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericStatement -> parse"));
        return false;
    }
    statement = temp;

    return true;
}


bool NodeGenericsCreate::parse() {
    AstNodeBase *temp = new NodeGenericCreate(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericsCreate -> NodeGenericCreate"));
        return false;
    }
    variables.push_back(temp);
    while (tokenManager->peek()->getType() == Token::Comma) {
        match(Token::Comma);
        temp = new NodeGenericCreate(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericsCreate -> NodeGenericCreate"));
            return false;
        }
        variables.push_back(temp);
    }
    return true;
}


bool NodeGenericCreate::parse() {
    AstNodeBase *temp = new NodeIdentifier(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericCreate -> NodeIdentifier"));
        return false;
    }
    identifier = temp;

    if (tokenManager->peek()->getType() == Token::OperatorAssignment) {
        match(Token::OperatorAssignment);
        temp = new NodeCollection(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericCreate -> NodeCollection"));
            return false;
        }
        collection = temp;
    }
    return true;
}


bool NodeGenericDecl::parse() {
    match(Token::LessThan);
    AstNodeBase *temp = new NodeType(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericDecl -> NodeType"));
        return false;
    }
    type = temp;
    match(Token::GreaterThan);

    temp = new NodeIdentifier(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeGenericDecl -> NodeIdentifier"));
        return false;
    }
    identifier = temp;
    return true;
}


bool NodeCollection::parse() {
    Token::Type end;
    switch (tokenManager->peek()->getType()) {
        case Token::LFigured:
            end = Token::RFigured;
            match(Token::LFigured);
            break;
        case Token::LBox:
            end = Token::RBox;
            match(Token::LBox);
            break;
        default:
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeCollection -> Brackets"));
            return false;
    }
    if (end == tokenManager->peek()->getType()) {
        match(end);
        return true;
    }
    AstNodeBase *temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeCollection -> NodeExpression"));
        return false;
    }
    variables.push_back(temp);
    while (tokenManager->peek()->getType() == Token::Comma) {
        match(Token::Comma);
        temp = new NodeExpression(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeCollection -> NodeExpression"));
            return false;
        }
        variables.push_back(temp);
    }
    if (end != tokenManager->peek()->getType()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeCollection -> Brackets"));
        return false;
    }
    match(end);
    return true;
}


bool NodeVariablesCreate::parse() {
    AstNodeBase *temp = new NodeVariableCreate(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariablesCreate -> NodeVariableCreate"));
        return false;
    }
    variables.push_back(temp);
    while (tokenManager->peek()->getType() == Token::Comma) {
        match(Token::Comma);
        temp = new NodeVariableCreate(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariablesCreate -> NodeVariableCreate"));
            return false;
        }
        variables.push_back(temp);
    }
    return true;
}


bool NodeVariableCreate::parse() {
    AstNodeBase *temp = new NodeIdentifier(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariableCreate -> NodeIdentifier"));
        return false;
    }
    identifier = temp;

    if (tokenManager->peek()->getType() == Token::OperatorAssignment) {
        match(Token::OperatorAssignment);
        temp = new NodeExpression(tokenManager, errors);
        if (!temp->parse()) {
            errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeVariableCreate -> NodeExpression"));
            return false;
        }
        value = temp;
    }
    return true;
}


bool NodeIfStatement::parse() {
    match(Token::KeywordIf);
    match(Token::LParen);
    AstNodeBase *temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeExpression"));
        return false;
    }
    expression = temp;
    match(Token::RParen);

    temp = new NodeBlock(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeBlock"));
        return false;
    }
    block = temp;

    if (tokenManager->peekUnsafe(0) != nullptr) {
        if (tokenManager->peek()->getType() == Token::KeywordElse) {
            match(Token::KeywordElse);
            temp = new NodeBlock(tokenManager, errors);
            if (!temp->parse()) {
                errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeBlock"));
                return false;
            }
            elseBlock = temp;
        }
    }

    return true;
}


bool NodeForStatement::parse() {
    match(Token::KeywordFor);
    match(Token::LParen);
    AstNodeBase *temp = nullptr;

    switch (tokenManager->peek()->getType()) {
        case Token::TypeString:
        case Token::TypeList:
        case Token::TypeInt:
        case Token::TypeDouble:
        case Token::TypeVoid:
        case Token::TypeBoolean:
        case Token::KeywordVar: {
            temp = new NodeTypesStatement(tokenManager, errors);
            if (!temp->parse()) {
                errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeTypesStatement"));
                return false;
            }
            variableDecl = temp;
            break;
        }
        case Token::Identifier: {
            temp = new NodeExpression(tokenManager, errors);
            if (!temp->parse()) {
                errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeExpression"));
                return false;
            }
            variableDecl = temp;
            break;
        }
        default:
            break;
    }
    match(Token::Semicolon);

    switch (tokenManager->peek()->getType()) {
        case Token::OperatorMinus:
        case Token::LParen:
        case Token::OperatorExclamationMark:
        case Token::OperatorPlusPlus:
        case Token::OperatorMinusMinus:
        case Token::DoubleValue:
        case Token::EmptyStringValue:
        case Token::BooleanValueFalse:
        case Token::BooleanValueTrue:
        case Token::NullValue:
        case Token::IntValue:
        case Token::Identifier: {
            temp = new NodeExpression(tokenManager, errors);
            if (!temp->parse()) {
                errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeExpression"));
                return false;
            }
            expression = temp;
            break;
        }
        default:
            break;
    }
    match(Token::Semicolon);

    switch (tokenManager->peek()->getType()) {
        case Token::OperatorMinus:
        case Token::LParen:
        case Token::OperatorExclamationMark:
        case Token::OperatorPlusPlus:
        case Token::OperatorMinusMinus:
        case Token::DoubleValue:
        case Token::EmptyStringValue:
        case Token::BooleanValueFalse:
        case Token::BooleanValueTrue:
        case Token::NullValue:
        case Token::IntValue:
        case Token::Identifier: {
            temp = new NodeExpression(tokenManager, errors);
            if (!temp->parse()) {
                errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeExpression"));
                return false;
            }
            expression = temp;
            break;
        }
        default:
            break;
    }
    match(Token::RParen);

    temp = new NodeBlock(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeIfStatement -> NodeBlock"));
        return false;
    }
    block = temp;
    return true;
}


bool NodeReturnStatement::parse() {
    match(Token::KeywordReturn);
    AstNodeBase *temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeReturnStatement -> NodeExpression"));
        return false;
    }
    expression = temp;
    return true;
}


bool NodeWhileStatement::parse() {
    match(Token::KeywordWhile);

    match(Token::LParen);
    AstNodeBase *temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeWhileStatement -> NodeExpression"));
        return false;
    }
    expression = temp;
    match(Token::RParen);

    temp = new NodeBlock(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeWhileStatement -> NodeBlock"));
        return false;
    }
    block = temp;
    return true;
}


bool NodeDoWhileStatement::parse() {
    match(Token::KeywordDo);

    AstNodeBase *temp = new NodeBlock(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDoWhileStatement -> NodeBlock"));
        return false;
    }
    block = temp;

    match(Token::KeywordWhile);

    match(Token::LParen);
    temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeDoWhileStatement -> NodeExpression"));
        return false;
    }
    expression = temp;
    match(Token::RParen);

    return true;
}


bool NodeAssignmentStatement::parse() {
    AstNodeBase *temp = nullptr;

    temp = new NodeAssignmentOperation(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(
                std::make_pair(tokenManager->peek(), "ERROR in NodeAssignmentStatement -> NodeAssignmentOperation"));
        return false;
    }
    operation = temp;

    temp = new NodeExpression(tokenManager, errors);
    if (!temp->parse()) {
        errors.insert(std::make_pair(tokenManager->peek(), "ERROR in NodeAssignmentStatement -> NodeExpression"));
        return false;
    }
    expression = temp;
    return true;
}










































//--------------------------------Accepts----------------------------------------------------

void *NodeBlock::accept(Visitor *v) { return v->visit(this); }

void *NodeExpression::accept(Visitor *v) { return v->visit(this); }

void *NodeSubExpression::accept(Visitor *v) { return v->visit(this); }

void *NodeAdditiveExpression::accept(Visitor *v) { return v->visit(this); }

void *NodeUnaryPrefixExpression::accept(Visitor *v) { return v->visit(this); }

void *NodeFactor::accept(Visitor *v) { return v->visit(this); }

void *NodeTerm::accept(Visitor *v) { return v->visit(this); }

void *NodeFunctionDecl::accept(Visitor *v) { return v->visit(this); }

void *NodeFunctionCall::accept(Visitor *v) { return v->visit(this); }

void *NodeCallParams::accept(Visitor *v) { return v->visit(this); }

void *NodeDeclParams::accept(Visitor *v) { return v->visit(this); }

void *NodeDeclParam::accept(Visitor *v) { return v->visit(this); }

void *NodeType::accept(Visitor *v) { return v->visit(this); }

void *NodeLiteral::accept(Visitor *v) { return v->visit(this); }

void *NodeIdentifier::accept(Visitor *v) { return v->visit(this); }

void *NodeMultiplicativeOperation::accept(Visitor *v) { return v->visit(this); }

void *NodeAdditiveOperation::accept(Visitor *v) { return v->visit(this); }

void *NodeRelationalOperation::accept(Visitor *v) { return v->visit(this); }

void *NodeAssignmentOperation::accept(Visitor *v) { return v->visit(this); }

void *NodeUnaryPostfixOperation::accept(Visitor *v) { return v->visit(this); }

void *NodeProgramState::accept(Visitor *v) { return v->visit(this); }

void *NodeImports::accept(Visitor *v) { return v->visit(this); }

void *NodeImport::accept(Visitor *v) { return v->visit(this); }

void *NodeStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeRootStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeTypesStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeVariableStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeGenericStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeGenericsCreate::accept(Visitor *v) { return v->visit(this); }

void *NodeGenericCreate::accept(Visitor *v) { return v->visit(this); }

void *NodeCollection::accept(Visitor *v) { return v->visit(this); }

void *NodeVariablesCreate::accept(Visitor *v) { return v->visit(this); }

void *NodeVariableCreate::accept(Visitor *v) { return v->visit(this); }

void *NodeIfStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeForStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeReturnStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeWhileStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeDoWhileStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeAssignmentStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeIdentifierStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeFieldCall::accept(Visitor *v) { return v->visit(this); }

void *NodeCallStatement::accept(Visitor *v) { return v->visit(this); }

void *NodeUnaryPostfixExpression::accept(Visitor *v) { return v->visit(this); }

void *NodeGenericDecl::accept(Visitor *v) { return v->visit(this); }

