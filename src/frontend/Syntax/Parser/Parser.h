#pragma once

#include <frontend/Lex/Token/Token.h>
#include <vector>
#include <map>
#include <frontend/Syntax/TokenManager/TokenManager.h>
#include <frontend/Syntax/AstTree/AstNodes.h>

class Parser {
public:
    explicit Parser(std::vector<Token> *resultLexer);

    ~Parser();

    void parse();

    AstNodeBase *getAst();

    unsigned int getIndex();

    std::map<Token*, std::string> getErrors();
    unsigned int getCountErrors();
private:
    std::map<Token*, std::string> errors;
    TokenManager *tokenManager;
    AstNodeBase *Tree;
};
