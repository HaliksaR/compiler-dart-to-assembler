#include "Parser.h"

//https://ps-group.github.io/compilers/ast
// https://stackoverflow.com/questions/3578083/what-is-the-best-way-to-use-a-hashmap-in-c
// https://stackoverflow.com/questions/18188612/abstract-syntax-tree-representation-in-c/18189053
// https://stackoverflow.com/questions/28530284/generate-an-ast-in-c
// https://ru.cppreference.com/w/cpp/container/unordered_map
// https://en.wikipedia.org/wiki/Abstract_syntax_tree
// https://scottmcpeak.com/elkhound/sources/elsa/

//https://stackoverflow.com/questions/24925356/how-can-i-parse-c-to-create-an-ast
// https://blog.klipse.tech/javascript/2017/02/08/tiny-compiler-parser.html


// https://medium.com/basecs/leveling-up-ones-parsing-game-with-asts-d7a6fc2400ff

Parser::Parser(std::vector<Token> *resultLexer) {
    tokenManager = new TokenManager(resultLexer);
    Tree = new NodeProgramState(tokenManager, errors);
}

Parser::~Parser() {
    free(tokenManager);
    free(Tree);
    errors.clear();
}

void Parser::parse() {
    Tree->parse();
}

AstNodeBase *Parser::getAst() {
    return Tree;
}

unsigned int Parser::getIndex() {
    return tokenManager->getPosition();
}

std::map<Token *, std::string> Parser::getErrors() { return errors; }

unsigned int Parser::getCountErrors() { return errors.size(); }
