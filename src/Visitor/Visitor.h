#pragma once


#include <frontend/Syntax/AstTree/AstNodes.h>
#include <sstream>

class AstNodeBase;

class NodeBlock;

class NodeExpression;

class NodeSubExpression;

class NodeAdditiveExpression;

class NodeUnaryPrefixExpression;

class NodeFactor;

class NodeTerm;

class NodeFunctionCall;

class NodeCallParams;

class NodeType;

class NodeLiteral;

class NodeIdentifier;

class NodeMultiplicativeOperation;

class NodeAdditiveOperation;

class NodeRelationalOperation;

class NodeProgramState;

class NodeRootStatement;

class NodeImports;

class NodeImport;

class NodeFunctionDecl;

class NodeDeclParams;

class NodeDeclParam;

class NodeVariableCreate;

class NodeStatement;

class NodeTypesStatement;

class NodeVariableStatement;

class NodeGenericStatement;

class NodeVariablesCreate;

class NodeGenericsCreate;

class NodeGenericCreate;

class NodeCollection;

class NodeIfStatement;

class NodeForStatement;

class NodeReturnStatement;

class NodeAssignmentOperation;

class NodeUnaryPostfixOperation;

class NodeAssignmentStatement;

class NodeWhileStatement;

class NodeDoWhileStatement;

class NodeIdentifierStatement;

class NodeCallStatement;

class NodeFieldCall;

class NodeUnaryPostfixExpression;

class NodeGenericDecl;

class Visitor {
public:
    Visitor() = default;

    virtual ~Visitor() = default;

    virtual void *visit(AstNodeBase *) = 0;

    virtual void *visit(NodeBlock *) = 0;

    virtual void *visit(NodeExpression *) = 0;

    virtual void *visit(NodeSubExpression *) = 0;

    virtual void *visit(NodeAdditiveExpression *) = 0;

    virtual void *visit(NodeUnaryPrefixExpression *) = 0;

    virtual void *visit(NodeFactor *) = 0;

    virtual void *visit(NodeTerm *) = 0;

    virtual void *visit(NodeFunctionDecl *) = 0;

    virtual void *visit(NodeFunctionCall *) = 0;

    virtual void *visit(NodeCallParams *) = 0;

    virtual void *visit(NodeDeclParams *) = 0;

    virtual void *visit(NodeDeclParam *) = 0;

    virtual void *visit(NodeType *) = 0;

    virtual void *visit(NodeLiteral *) = 0;

    virtual void *visit(NodeIdentifier *) = 0;

    virtual void *visit(NodeMultiplicativeOperation *) = 0;

    virtual void *visit(NodeAdditiveOperation *) = 0;

    virtual void *visit(NodeRelationalOperation *) = 0;

    virtual void *visit(NodeAssignmentOperation *) = 0;

    virtual void *visit(NodeUnaryPostfixOperation *) = 0;

    virtual void *visit(NodeProgramState *) = 0;

    virtual void *visit(NodeImports *) = 0;

    virtual void *visit(NodeImport *) = 0;

    virtual void *visit(NodeStatement *) = 0;

    virtual void *visit(NodeRootStatement *) = 0;

    virtual void *visit(NodeTypesStatement *) = 0;

    virtual void *visit(NodeVariableStatement *) = 0;

    virtual void *visit(NodeGenericStatement *) = 0;

    virtual void *visit(NodeGenericsCreate *) = 0;

    virtual void *visit(NodeGenericCreate *) = 0;

    virtual void *visit(NodeCollection *) = 0;

    virtual void *visit(NodeVariablesCreate *) = 0;

    virtual void *visit(NodeVariableCreate *) = 0;

    virtual void *visit(NodeIfStatement *) = 0;

    virtual void *visit(NodeForStatement *) = 0;

    virtual void *visit(NodeReturnStatement *) = 0;

    virtual void *visit(NodeWhileStatement *) = 0;

    virtual void *visit(NodeDoWhileStatement *) = 0;

    virtual void *visit(NodeAssignmentStatement *) = 0;

    virtual void *visit(NodeIdentifierStatement *) = 0;

    virtual void *visit(NodeFieldCall *) = 0;

    virtual void *visit(NodeCallStatement *) = 0;

    virtual void *visit(NodeUnaryPostfixExpression *) = 0;
    virtual void *visit(NodeGenericDecl *) = 0;
};


class ParserVisitor : virtual public Visitor {
public:
    ParserVisitor() : Visitor() {};

    ~ParserVisitor() override = default;

    std::string show() { return buffer.str(); }

private:
    std::stringstream buffer;

    unsigned int Tabs = 0;

    std::string tabs() const;

    void *visit(AstNodeBase *node) override { return nullptr; };

    void *visit(NodeBlock *node) override;

    void *visit(NodeExpression *node) override;

    void *visit(NodeSubExpression *node) override;

    void *visit(NodeAdditiveExpression *node) override;

    void *visit(NodeUnaryPrefixExpression *node) override;

    void *visit(NodeFactor *node) override;

    void *visit(NodeTerm *node) override;

    void *visit(NodeFunctionDecl *node) override;

    void *visit(NodeFunctionCall *node) override;

    void *visit(NodeCallParams *node) override;

    void *visit(NodeDeclParams *node) override;

    void *visit(NodeDeclParam *node) override;

    void *visit(NodeType *node) override;

    void *visit(NodeLiteral *node) override;

    void *visit(NodeIdentifier *node) override;

    void *visit(NodeMultiplicativeOperation *node) override;

    void *visit(NodeAdditiveOperation *node) override;

    void *visit(NodeRelationalOperation *node) override;

    void *visit(NodeAssignmentOperation *node) override;

    void *visit(NodeUnaryPostfixOperation *node) override;

    void *visit(NodeProgramState *node) override;

    void *visit(NodeImports *node) override;

    void *visit(NodeImport *node) override;

    void *visit(NodeStatement *node) override;

    void *visit(NodeRootStatement *node) override;

    void *visit(NodeTypesStatement *node) override;

    void *visit(NodeVariableStatement *node) override;

    void *visit(NodeGenericStatement *node) override;

    void *visit(NodeGenericsCreate *node) override;

    void *visit(NodeGenericCreate *node) override;

    void *visit(NodeCollection *node) override;

    void *visit(NodeVariablesCreate *node) override;

    void *visit(NodeVariableCreate *node) override;

    void *visit(NodeIfStatement *node) override;

    void *visit(NodeForStatement *node) override;

    void *visit(NodeReturnStatement *node) override;

    void *visit(NodeWhileStatement *node) override;

    void *visit(NodeDoWhileStatement *node) override;

    void *visit(NodeAssignmentStatement *node) override;

    void *visit(NodeIdentifierStatement *node) override;

    void *visit(NodeFieldCall *node) override;

    void *visit(NodeCallStatement *node) override;

    void *visit(NodeUnaryPostfixExpression *node) override;
    void *visit(NodeGenericDecl *node) override;
};
