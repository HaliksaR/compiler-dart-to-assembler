#include <main/colors.h>
#include "Visitor.h"


std::string ParserVisitor::tabs() const {
    if (buffer.str().back() == ' ') return "";
    std::string ret;
    for (unsigned int i = 0; i < Tabs; i++)
        ret.append("|    ");
    return ret;
}

void *ParserVisitor::visit(NodeBlock *node) {
    buffer << std::endl << tabs() << YELLOW << "<Block>" << RESET;
    Tabs++;
    for (auto &statement : node->statements)
        statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << YELLOW << "</Block>" << RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeExpression *node) {
    unsigned int factors = 0;
    for (unsigned int i = 0; i < node->relationalOperations.size(); ++i) {
        buffer << std::endl << tabs() << "<Relational&EqualityExpr ";
        node->relationalOperations.at(i)->accept(this);
        buffer << ">";
        Tabs++;
        node->additiveExpressions.at(factors++)->accept(this);
    }
    node->additiveExpressions.at(factors++)->accept(this);
    for (unsigned int i = 0; i < node->relationalOperations.size(); ++i) {
        Tabs--;
        buffer << std::endl << tabs() << "</Relational&EqualityExpr>";
    }
    return nullptr;
}

void *ParserVisitor::visit(NodeSubExpression *node) {
    buffer << std::endl << tabs() << "<Sub>";
    Tabs++;
    node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Sub>";
    return nullptr;
}

void *ParserVisitor::visit(NodeAdditiveExpression *node) {
    unsigned int terms = 0;
    for (unsigned int i = 0; i < node->additiveOP.size(); i++) {
        buffer << std::endl << tabs() << "<AdditiveExpr ";
        node->additiveOP.at(i)->accept(this);
        buffer << ">";
        Tabs++;
        node->terms.at(terms++)->accept(this);
    }
    node->terms.at(terms++)->accept(this);
    for (unsigned int i = 0; i < node->additiveOP.size(); i++) {
        Tabs--;
        buffer << std::endl << tabs() << "</AdditiveExpr>";
    }
    return nullptr;
}

void *ParserVisitor::visit(NodeUnaryPrefixExpression *node) {
    buffer << std::endl << tabs() << "<UnaryPrefixExpr operation=\"";
    buffer << GREEN << node->token->getLexeme() << RESET;
    buffer << "\">";
    Tabs++;
    node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</UnaryPrefixExpr>";
    return nullptr;
}

void *ParserVisitor::visit(NodeUnaryPostfixExpression *node) {
    buffer << std::endl << tabs() << "<UnaryPostfixExpr operation=\"";
    buffer << GREEN << node->token->getLexeme() << RESET;
    buffer << "\">";
    Tabs++;
    node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</UnaryPostfixExpr>";
    return nullptr;
}

void *ParserVisitor::visit(NodeFactor *node) {
    buffer << std::endl << tabs() << "<Factor>";
    Tabs++;
    if (node->node != nullptr)
        node->node->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Factor>";
    return nullptr;
}

void *ParserVisitor::visit(NodeTerm *node) {
    unsigned int factors = 0;
    for (unsigned int i = 0; i < node->multiplicativeOP.size(); ++i) {
        buffer << std::endl << tabs() << "<Term ";
        node->multiplicativeOP.at(i)->accept(this);
        buffer << ">";
        Tabs++;
        node->factors.at(factors++)->accept(this);
    }
    node->factors.at(factors++)->accept(this);
    for (unsigned int i = 0; i < node->multiplicativeOP.size(); ++i) {
        Tabs--;
        buffer << std::endl << tabs() << "</Term>";
    }
    return nullptr;
}

void *ParserVisitor::visit(NodeFunctionCall *node) {
    buffer << std::endl << tabs() << MAGENTA << "<FunctionCall>" << RESET;
    Tabs++;
    if (node->actualParams)
        node->actualParams->accept(this);
    if (node->expression)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << MAGENTA << "</FunctionCall>" << RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeCallParams *node) {
    for (auto &expression : node->expressions) {
        buffer << std::endl << tabs() << "<CallParams>";
        Tabs++;
        expression->accept(this);
        Tabs--;
        buffer << std::endl << tabs() << "</CallParams>";
    }
    return nullptr;
}

void *ParserVisitor::visit(NodeType *node) {
    buffer << " type=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeLiteral *node) {
    std::string value = "Nothing";
    switch (node->token->getType()) {
        case Token::BooleanValueFalse:
        case Token::BooleanValueTrue:
            value = "Bool";
            break;
        case Token::IntValue:
            value = "Int";
            break;
        case Token::NullValue:
            value = "Null";
            break;
        case Token::EmptyStringValue:
        case Token::StringValue:
            value = "String";
            break;
        case Token::DoubleValue:
            value = "Double";
            break;
        default:
            break;
    }
    buffer << std::endl << tabs() << "<" + value + ">" << GREEN << node->token->getLexeme() << RESET
           << "</" + value + ">";
    return nullptr;
}

void *ParserVisitor::visit(NodeIdentifier *node) {
    buffer << std::endl << tabs() << "<Identifier>" << GREEN << node->token->getLexeme() << RESET << "</Identifier>";
    return nullptr;
}

void *ParserVisitor::visit(NodeMultiplicativeOperation *node) {
    buffer << "operation=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeAdditiveOperation *node) {
    buffer << "operation=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeRelationalOperation *node) {
    buffer << "operation=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeProgramState *node) {
    buffer << std::endl << tabs() << RED << "<Program>" << RESET;
    Tabs++;
    if (node->imports != nullptr)
        node->imports->accept(this);
    for (auto &statement : node->statements)
        statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</Program>" << RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeImports *node) {
    buffer << std::endl << tabs() << "<Imports>";
    Tabs++;
    for (auto &import : node->imports)
        import->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Imports>";
    return nullptr;
}

void *ParserVisitor::visit(NodeImport *node) {
    buffer << std::endl << tabs() << "<Import lib=\"" << node->lib->getLexeme() << "\">";
    return nullptr;
}


void *ParserVisitor::visit(NodeRootStatement *node) {
    buffer << std::endl << tabs() << YELLOW << "<RootStatement>" << RESET;
    Tabs++;
    if (node->statement != nullptr)
        node->statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << YELLOW << "</RootStatement>" << RESET;
    return nullptr;
}


void *ParserVisitor::visit(NodeFunctionDecl *node) {
    buffer << std::endl << tabs() << "<FunctionDecl>";
    Tabs++;
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    if (node->formalParams != nullptr)
        node->formalParams->accept(this);
    if (node->block != nullptr)
        node->block->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</FunctionDecl>";
    return nullptr;
}

void *ParserVisitor::visit(NodeDeclParams *node) {
    buffer << std::endl << tabs() << "<Params>";
    Tabs++;
    for (auto &param : node->params)
        param->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Params>";
    return nullptr;
}

void *ParserVisitor::visit(NodeDeclParam *node) {
    buffer << std::endl << tabs() << "<Param";
    node->type->accept(this);
    buffer << ">";
    Tabs++;
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Param>";
    return nullptr;
}

void *ParserVisitor::visit(NodeVariableCreate *node) {
    buffer << std::endl << tabs() << "<VarCreate>";
    Tabs++;
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    if (node->value != nullptr)
        node->value->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</VarCreate>";
    return nullptr;
}

void *ParserVisitor::visit(NodeStatement *node) {
    buffer << std::endl << tabs() << "<Statement>";
    Tabs++;
    if (node->statement != nullptr)
        node->statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Statement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeTypesStatement *node) {
    buffer << std::endl << tabs() << "<TypesStatement";
    if (node->type != nullptr)
        node->type->accept(this);
    buffer << ">";
    Tabs++;
    if (node->statement != nullptr)
        node->statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</TypesStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeVariableStatement *node) {
    buffer << std::endl << tabs() << "<VariableStatement>";
    Tabs++;
    if (node->statement != nullptr)
        node->statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</VariableStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeGenericStatement *node) {
    buffer << std::endl << tabs() << "<GenericStatement";
    node->type->accept(this);
    buffer << ">";
    Tabs++;
    if (node->statement != nullptr)
        node->statement->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</GenericStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeVariablesCreate *node) {
    buffer << std::endl << tabs() << "<VariablesCreate>";
    Tabs++;
    for (auto &variable : node->variables)
        variable->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</VariablesCreate>";
    return nullptr;
}

void *ParserVisitor::visit(NodeGenericsCreate *node) {
    buffer << std::endl << tabs() << "<GenericsCreate>";
    Tabs++;
    for (auto &variable : node->variables)
        variable->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</GenericsCreate>";
    return nullptr;
}

void *ParserVisitor::visit(NodeGenericCreate *node) {
    buffer << std::endl << tabs() << "<GenericCreate>";
    Tabs++;
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    if (node->collection != nullptr)
        node->collection->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</GenericCreate>";
    return nullptr;
}

void *ParserVisitor::visit(NodeCollection *node) {
    buffer << std::endl << tabs() << "<Collection>";
    Tabs++;
    for (auto &variable : node->variables)
        variable->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</Collection>";
    return nullptr;
}

void *ParserVisitor::visit(NodeIfStatement *node) {
    buffer << std::endl << tabs() << RED << "<If>"<< RESET;
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    if (node->block != nullptr)
        node->block->accept(this);
    if (node->elseBlock != nullptr)
        node->elseBlock->accept(this);
    if (node->elseIfBlock != nullptr)
        node->elseIfBlock->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</If>"<< RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeForStatement *node) {
    buffer << std::endl << tabs() << RED << "<For>"<< RESET;
    Tabs++;
    if (node->variableDecl != nullptr)
        node->variableDecl->accept(this);
    if (node->expression != nullptr)
        node->expression->accept(this);
    if (node->assignment != nullptr)
        node->assignment->accept(this);
    if (node->block != nullptr)
        node->block->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</For>"<< RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeReturnStatement *node) {
    buffer << std::endl << tabs() << RED << "<Return>"<< RESET;
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</Return>"<< RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeAssignmentOperation *node) {
    buffer << "operation=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeUnaryPostfixOperation *node) {
    buffer << "operation=\"" << GREEN << node->token->getLexeme() << RESET << "\"";
    return nullptr;
}

void *ParserVisitor::visit(NodeWhileStatement *node) {
    buffer << std::endl << tabs() << RED << "<While>"<< RESET;
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    if (node->block != nullptr)
        node->block->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</While>"<< RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeDoWhileStatement *node) {
    buffer << std::endl << tabs() << RED << "</DoWhile>"<< RESET;
    Tabs++;
    if (node->block != nullptr)
        node->block->accept(this);
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << RED << "</DoWhile>"<< RESET;
    return nullptr;
}

void *ParserVisitor::visit(NodeAssignmentStatement *node) {
    buffer << std::endl << tabs() << "<AssignmentStatement";
    if (node->operation != nullptr) {
        buffer << " ";
        node->operation->accept(this);
    }
    buffer << ">";
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</AssignmentStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeIdentifierStatement *node) {
    buffer << std::endl << tabs() << "<IdentifierStatement>";
    Tabs++;
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</IdentifierStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeCallStatement *node) {
    buffer << std::endl << tabs() << "<NodeCallStatement>";
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</NodeCallStatement>";
    return nullptr;
}

void *ParserVisitor::visit(NodeFieldCall *node) {
    buffer << std::endl << tabs() << "<FieldCall>";
    Tabs++;
    if (node->expression != nullptr)
        node->expression->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</FieldCall>";
    return nullptr;
}

void *ParserVisitor::visit(NodeGenericDecl *node) {
    buffer << std::endl << tabs() << "<GenericDecl";
    Tabs++;
    node->type->accept(this);
    buffer << ">";
    if (node->identifier != nullptr)
        node->identifier->accept(this);
    Tabs--;
    buffer << std::endl << tabs() << "</GenericDecl>";
    return nullptr;
}



