#pragma once

#include <string>
#include <list>
#include "enums/Status.h"
#include "enums/Dump.h"

Status read_args(int argc, const char **argv, std::string &path, std::vector<Dump> &dump_state);

Dump resolveOption(const std::string &input);