#include <iostream>
#include <vector>
#include "OptionsParser.h"

Dump resolveOption(const std::string &input) {
    if (input == "--dump-tokens=not-comments") return Dump::DUMP_TOKENS_NOT_COMMENTS;
    if (input == "--dump-tokens") return Dump::DUMP_TOKENS;
    if (input == "--dump-tokens=save") return Dump::DUMP_TOKENS_SAVE;
    if (input == "--dump-ast") return Dump::DUMP_AST;
    if (input == "--dump-ast=save") return Dump::DUMP_AST_SAVE;
    if (input == "--dump-asm") return Dump::DUMP_ASM;
    return Dump::NOTHING;
}

Status read_args(int argc, const char **argv, std::string &path, std::vector<Dump> &dump_state) {
    if ((std::string) argv[1] == "--help") {
        std::cout << "Options:\n"
                     "--dump-tokens — вывести результат работы лексического анализатора\n"
                     "\tParameters:\n"
                     "\t =not-comments  — вывод без комментариев\n"
                     "\t =save  — вывод токенов в файл\n"
                     "\t example: --dump-tokens=not-comments\n"
                     "--dump-ast — вывести AST\n"
                     "\t =save  — вывод дерева в виде xml в файл\n"
                     "--dump-asm — вывести ассемблер"
                  << "\n";
        return Status::ERROR;
    }
    if (argc - 1 <= 0 || resolveOption(argv[argc - 1]) != Dump::NOTHING) {
        std::cerr << "PLEASE INPUT PROGRAM" << "\n";
        return Status::ERROR;
    }
    path = argv[argc - 1];
    for (int i = 1; i < argc - 1; i++) {
        switch (resolveOption(argv[i])) {
            case Dump::DUMP_TOKENS:
                dump_state.push_back(Dump::DUMP_TOKENS);
                break;
            case Dump::DUMP_TOKENS_NOT_COMMENTS:
                dump_state.push_back(Dump::DUMP_TOKENS_NOT_COMMENTS);
                break;
            case Dump::DUMP_AST:
                dump_state.push_back(Dump::DUMP_AST);
                break;
            case Dump::DUMP_ASM:
                dump_state.push_back(Dump::DUMP_ASM);
                break;
            case Dump::DUMP_TOKENS_SAVE:
                dump_state.push_back(Dump::DUMP_TOKENS_SAVE);
                break;
            case Dump::DUMP_AST_SAVE:
                dump_state.push_back(Dump::DUMP_AST_SAVE);
                break;
            case Dump::NOTHING:
                std::cerr << "INVALID OPTIONS" << "\n";
                return Status::ERROR;

        }
    }
    return Status::SUCCESS;
}