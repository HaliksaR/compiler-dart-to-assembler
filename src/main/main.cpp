#include <iostream>
#include <vector>
#include <frontend/Lex/Lexer/Lexer.h>
#include <frontend/Lex/PrintDump/PrintDump.h>
#include <frontend/Syntax/Parser/Parser.h>
#include <algorithm>
#include <frontend/Syntax/AstTree/AstNodes.h>
#include <frontend/Syntax/PrintDump/PrintDump.h>
#include "enums/Status.h"
#include "enums/Dump.h"
#include "main/optionsParser/OptionsParser.h"
#include "colors.h"

// --dump-tokens // --dump-tokens=not-comments --dump-tokens=save --dump-ast --dump-ast=save --dump-asm
// ../../program/main.dart

int main(int argc, const char **argv) {
    std::ifstream file;
    std::vector<Dump> dump_state;
    {
        std::string path;
        Status status = read_args(argc, argv, path, dump_state);
        if (status == Status::ERROR)
            exit(EXIT_FAILURE);
        file.open(path);
        if (!file) {
            std::cerr << "Program not found" << "\n";
            exit(EXIT_FAILURE);
        }
    }

    auto *resultLexer = new std::vector<Token>();
    {
        std::cout << YELLOW << "Lexing..." << RESET << std::endl;
        auto *temp = new std::vector<Token>();
        auto *lexer = new Lexer(file);
        lexer->parse(temp);
        std::cout << GREEN << "Finished Lexing" << RESET << std::endl;
        int countUnknown;
        if (dump_lexer(temp, dump_state, countUnknown) == Status::ERROR) {
            std::cerr << "The program has " << countUnknown << " lexical errors" << std::endl;
            exit(EXIT_FAILURE);
        }
        for (auto & i : *temp)
            if (i.getType() != Token::CommentText)
                resultLexer->push_back(i);
        free(temp);
    }

    AstNodeBase *tree;
    {
        std::cout << YELLOW << "Parsing..." << RESET << std::endl;
        auto *parser = new Parser(resultLexer);
        parser->parse();
        unsigned int count = parser->getCountErrors();
        if (count != 0) {
            auto map = parser->getErrors();
            std::for_each(map.begin(), map.end(), [](const std::pair<Token *, std::string> &error) {
                std::cerr << error.second << std::endl;
                std::cerr << "->" << error.first->getLexeme() << "<- " << getPositions(error.first->getPosition()) << std::endl;
            });
            std::cerr << "count -> " << count << std::endl;
            exit(EXIT_FAILURE);
        }
        tree = parser->getAst();
        std::cout << GREEN << "Parsing successful" << RESET << std::endl;
        dump_parser(tree, dump_state);
    }

    //var https://docs.google.com/document/d/1O5q4blq9BI_NisVb-TXXrir7hGa5H4YIXQDPlsvRDm0/edit#heading=h.3br5s4xrlav2
    {
        //table
    }

    //var https://kryukova.engineer-software.ru/courses/compilers2020_1.html
    {
        //Semantic
    }

    //var
    {
        //CodeGenerate
    }
    return 0;
}