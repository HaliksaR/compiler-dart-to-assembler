#pragma once

#include <ostream>
#include "frontend/Lex/Token/Token.h"

std::ostream &operator<<(std::ostream &os, const Token::Type &chunk) {
    switch (chunk) {
        case Token::Type::Identifier :
            return os << "Identifier";

        case Token::Type::KeywordVar :
            return os << "KeywordVar";
        case Token::Type::KeywordIf :
            return os << "KeywordIf";
        case Token::Type::KeywordElse :
            return os << "KeywordElse";
        case Token::Type::KeywordFor :
            return os << "KeywordFor";
        case Token::Type::KeywordImport :
            return os << "KeywordImport";
        case Token::Type::KeywordReturn :
            return os << "KeywordReturn";

        case Token::Type::LParen :
            return os << "LParen";
        case Token::Type::RParen :
            return os << "RParen";

        case Token::Type::LFigured :
            return os << "LFigured";
        case Token::Type::RFigured :
            return os << "RFigured";

        case Token::Type::LBox :
            return os << "LBox";
        case Token::Type::RBox :
            return os << "RBox";

        case Token::Type::OperatorPlus :
            return os << "OperatorPlus";
        case Token::Type::OperatorMinus :
            return os << "OperatorMinus";
        case Token::Type::OperatorMultiply :
            return os << "OperatorMultiply";
        case Token::Type::OperatorDivision :
            return os << "OperatorDivision";
        case Token::Type::OperatorAssignment :
            return os << "OperatorDivision";
        case Token::Type::OperatorExclamationMark :
            return os << "OperatorExclamationMark";

        case Token::Type::LessThan :
            return os << "LessThan";
        case Token::Type::GreaterThan :
            return os << "GreaterThan";

        case Token::Type::Comma :
            return os << "Comma";
        case Token::Type::Dot :
            return os << "Dot";
        case Token::Type::Semicolon :
            return os << "Semicolon";
        case Token::Type::Colon :
            return os << "Colon";

        case Token::Type::TypeString :
            return os << "TypeString";
        case Token::Type::StringValue :
            return os << "StringValue";
        case Token::Type::EmptyStringValue :
            return os << "EmptyStringValue";
        case Token::Type::TypeList :
            return os << "TypeList";
        case Token::Type::TypeInt :
            return os << "TypeInt";
        case Token::Type::IntValue :
            return os << "IntValue";
        case Token::Type::TypeDouble :
            return os << "TypeDouble";
        case Token::Type::DoubleValue :
            return os << "DoubleValue";
        case Token::Type::TypeVoid :
            return os << "TypeVoid";
        case Token::Type::TypeBoolean :
            return os << "TypeBoolean";

        case Token::Type::CommentText :
            return os << "CommentText";
        case Token::Type::End :
            return os << "End";
        case Token::Type::NullValue :
            return os << "NullValue";
        case Token::LessThanOrEqualTo:
            return os << "LessThanOrEqualTo";
        case Token::EqualTo:
            return os << "EqualTo";
        case Token::NotEqualTo:
            return os << "NotEqualTo";
        case Token::AddAndAssignToSelf:
            return os << "AddAndAssignToSelf";
        case Token::SubtractAndAssignToSelf:
            return os << "SubtractAndAssignToSelf";
        case Token::MultiplyAndAssignToSelf:
            return os << "MultiplyAndAssignToSelf";
        case Token::DivideAndAssignToSelf:
            return os << "DivideAndAssignToSelf";
        case Token::GreaterThanOrEqualTo:
            return os << "GreaterThanOrEqualTo";
        case Token::OperatorPlusPlus:
            return os << "OperatorPlusPlus";
        case Token::OperatorMinusMinus:
            return os << "OperatorMinusMinus";
        case Token::BooleanValueTrue:
            return os << "BooleanValueTrue";
        case Token::BooleanValueFalse:
            return os << "BooleanValueFalse";
            return os << "Read";
        case Token::KeywordWhile:
            return os << "KeywordWhile";
        case Token::KeywordDo:
            return os << "KeywordDo";
        default:
            return os << "Unknown";

    }
}